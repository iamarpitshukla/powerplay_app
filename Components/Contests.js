import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    View,
    TextInput,
    Text,
    ProgressViewIOS,
    ProgressBarAndroid,
    Image,
    AsyncStorage,
    ScrollView,
    ActivityIndicator,
     Dimensions
} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
const data = require('../Dummy/Contest');
var widthD = Dimensions.get('window').width;
var heightD = Dimensions.get('window').height;
let h_ = heightD / 2
Props = {};
class Contests extends Component<Props> {

  static navigationOptions={
    headerStyle: {
      backgroundColor: 'white'
    },
     title: 'Contests',
  };
  
    constructor(props) {
    super(props);
    
     this.state={ 
        contestDTO: [],
        teamsquad1: '',
        teamsquad2: '',
        teamname2: '',
        teamname1: '',
        smallContest: [],
        bigContest: []
    };
  };

  componentWillMount() {
    const match = this.props.navigation.state.params.data;
    fetch('http://13.127.217.102:8000/getMatchDetails?unique_id='+match.matchId)
    .then((response)=> response.json())
    .then((res)=> {
      if(res.status !=200){
            alert(res.message)
            return
      }
      this.setState({ contestDTO: res.contestDTOs,
                      teamsquad1: res.squad[0].players,
                      teamsquad2: res.squad[1].players,
                      teamname1:  res.squad[0].name,
                      teamname2:  res.squad[1].name
      });
    });
  }

  secondsToString( timeRemaining ) {
    // timeRemaining = timeRemaining / 1000000
    var date = String(new Date(timeRemaining));
    return date;
    // alert(date)
    // return (setInterval(()=> {
    //       this.calculate(timeRemaining)
    //     }, 1000));
  }

  truncate(string) {
      if (string.length >= 10) return string.substring(0, 10) + '...';
      else return string;
  }

  renderContests() {
          const match = this.props.navigation.state.params.data;
    return this.state.contestDTO.map((contest, idx)=> {
      return (<View key= {idx} style= {{ width: widthD-25, backgroundColor: '#ecf0f1', elevation: 4, height: 100, margin: 1}}>
                  <View style= {{flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center', backgroundColor: 'white', padding: 4, margin: 1}}>
                    <Text style= {{fontSize: 16}}>
                      {contest.displayContest}
                    </Text>
                  <View style= {{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                    <View style= {{flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginHorizontal: 10}}>
                      <Text style= {{alignSelf: 'center', fontSize: 12}}>
                        Prize Money
                      </Text>
                      <Text style= {{alignSelf: 'center', fontSize: 12, color: '#44bd32'}}>
                        {contest.totalPrize} 
                      </Text>
                    </View>
                    <View style= {{flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginHorizontal: 10}}>
                      <Text style= {{alignSelf: 'center', fontSize: 12}}>
                        Winners
                      </Text>
                      <Text style= {{alignSelf: 'center', fontSize: 12, color: '#44bd32'}}>
                        {contest.totalCount}
                      </Text>
                    </View>
                    <View style= {{flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginHorizontal: 10}}>
                      <Text style= {{alignSelf: 'center', fontSize: 12}}>
                        Entry Fees
                      </Text>
                      <View style= {{flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center'}}>
                      <FontAwesome5 name= "rupee-sign" size= {14} color="green" />
                      <Text style= {{alignSelf: 'center', fontSize: 12, color: '#44bd32'}}>
                        {contest.entryFees} 
                      </Text>
                      </View>
                    </View>
                      <TouchableOpacity 
                        onPress= {()=>this.props.navigation.navigate('ContestDetails', {data: match, contestname: contest})}
                        style= {{justifyContent: 'center', alignItems: 'center', width: 80, height: 20, backgroundColor: '#44bd32', borderRadius: 18, margin: 2, padding: 2, elevation: 4}}>
                        <Text>Join Now !
                        </Text>
                      </TouchableOpacity>
                    </View>

                    <ProgressViewIOS
                         style= {{width: '80%',padding:5,margin:5}}
                      styleAttr="Horizontal"
                      indeterminate={false}
                      progress={contest.playingCount / contest.totalCount}
                    />
                  <View style= {{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '100%'}}>
                      <Text style= {{alignSelf: 'flex-start', fontSize: 8}}>
                      {contest.totalCount - contest.playingCount} spots left!
                      </Text>
                      <Text style= {{alignSelf: 'flex-end', fontSize: 8}}>
                        {contest.totalCount} Teams
                      </Text>
                  </View>                           
                 </View>
                  </View>
              );
    })
  }

    render() {
      const match = this.props.navigation.state.params.data;
        return (
          <View style={{flex:1, justifyContent: 'center', flexDirection: 'column', alignItems: 'center'}}>       
                <View style= {styles.innerContainer}>
                  <View style= {styles.headerView}>
                      <View style= {{flexDirection: 'row', justifyContent: 'space-around'}}>
                        <Text style= {styles.teamText}>
                          {this.truncate(match.team1ShortName)}
                        </Text>
                        <Text style= {styles.teamText}>
                          Vs.
                        </Text>
                        <Text style= {styles.teamText}>
                          {this.truncate(match.team2ShortName)}
                        </Text>
                      </View>
                      <View style= {{flexDirection: 'row', justifyContent: 'space-around'}}>
                        <Text>{this.secondsToString(match.startTime)}</Text>
                      </View>
                  </View>
                  <View style= {styles.pickerView}>
                    <TouchableOpacity style= {styles.tabButton}> 
                      <Text>             
                        All Contests
                      </Text>             
                  </TouchableOpacity>
                 </View> 
                 <ScrollView showsVerticalScrollIndicator= {false}>
                    {
                      this.renderContests()
                    }
              </ScrollView>
                </View>
          </View> 
    );
    }
}

const styles = StyleSheet.create({
    innerContainer: {
      flex : 1,
      width: '100%',
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'flex-start',
      flexDirection: 'column',
      padding: 2,
    },
    headerView: {
      padding: 6,
      width: '100%',
      elevation: 4,
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center'  
    },
    lowerLogin: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    submitButtonPlay: { 
      padding: 10, 
      backgroundColor: 'red', 
      width : 100,
      height: 100,
      elevation : 4,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 50
    },
    submitButton: {
      margin: 10,
      backgroundColor: 'white', 
      width : '45%',
      height: 40,
      elevation : 2,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 4
    },
    submitButtonText: { 
      color: 'red', 
      fontWeight: 'bold', 
      textAlign: 'center'
    },
    submitButtonTextPlay: { 
      color: 'white', 
      fontWeight: 'bold', 
      textAlign: 'center'
    },
    teamText: {
      fontSize: 18,
      alignSelf: 'center',
      fontWeight: 'bold',
      margin: 8
    },
    chooseText: {
      fontSize: 18,
      fontWeight: 'bold',
      margin: 1
    },
    chooseTextHead: {
      fontSize: 18,
      fontWeight: 'bold',
      color: '#ff9f43',
      margin: 8
    },
    chooseTextHead1: {
      fontSize: 18,
      fontWeight: 'bold',
      color: 'green',
      margin: 8
    },
    tabButton: {
      width: '100%', 
      height: 30, 
      padding: 4, 
      elevation: 4, 
      backgroundColor: '#dcdde1', 
      justifyContent: 'center', 
      alignItems: 'center'
    },
    pickerView: {
      flexDirection: 'row',
      justifyContent: 'center',
      width: '100%',
      alignItems: 'center'
    },
});

export default Contests;